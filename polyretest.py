import re

def getNumbers(polynomial):
    regex = re.compile(r"([-]?\d+)?[x]?\^?(\d+)?") # regex to get numbers
    #regex = re.compile(r"([-]?\d+)?([x])?\^?(\d+)?")
    #regex=re.compile(r"(\d)")
    regex = regex.findall(polynomial) # get all matches
    filter(None, regex)
    regexfn = [sublist for sublist in regex if any(sublist)]
    # for i in regex:
        # if i == " ('', '', '')":
            # regex.remove(i)   
    return regexfn

def getPowersAndCoefficients(getNums):
    L = getNums
    coefficients = [x[0] for x in L] # gives the coefficients
    powers = [x[1] for x in L] # gives the powers

    return coefficients # now we can get the coefficients and powers to add

print(getNumbers('2x^3+4x^2+8x-16'))
l = getNumbers('2x^3+4x^2+8x-16')
print(getPowersAndCoefficients(l))