from unittest import TestCase
from bowling_game import BowlingGame

class TestBowlingGame(TestCase):
    def setUp():
        self.game = BowlingGame()

    def roll_lots(self, rolls, pins):
        for roll in range(rolls):
            self.game.roll(pins)

    def test_gutter_game(self):
        self.roll_lots(20, 0)
        self.assertEqual(self.game(), 0)

    def test_all_ones_game(self):
        self.roll_lots(20, 1)
        self.assertEqual(self.game.score(), 20) 

    def text_mixed_rolls(self):
        self.roll_lots(5, 1)
        self.roll_lots(5, 2)
        self.roll_lots(11, 0)
        self.assertEqual(self.game.score(), 13)
        
    def test_roll_one_spare(self):
        self.game.roll(7)
        self.game.roll(3) # Spare
        self.roll_lots(18, 2)
        self.assertEqual(self.game.score(), 48)

    def test_roll_one_strike(self):
        self.game.roll(10)
        self.game.roll(5)
        self.game.roll(4)
        self.roll_lots(18, 0)
        self.assertEqual(self.game.score(), 28)
