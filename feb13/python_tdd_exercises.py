def reverse_list(lst):
    """
    Reverses order of elements in list lst.
    """
    reversed = []
    for item in lst:
        reversed = [item] + reversed
    return reversed

def reverse_string(s):
    return s[::-1]

def is_english_vowel(c):
    if c in ("aeiyouAEIYOU"):
        return True

def count_num_vowels(d):
    counter = 0
    for i in d:
        if i in ("aeiyouAEIYOU"):
            counter += 1
    return counter

def histogram(e):
    printit = ("")
    for i in e:
        printit += i * "#"
        if i != e[-1]:
            printit += "\n"
    return printit

def get_word_lengths(f):
    wordlens = []
    words = f.split(" ")
    for i in words:
        wordlens.append(len(i))
    return wordlens

def find_longest_word(g):
    words = g.split(" ")
    return max(words, key=len)

def validate_dna(h):
    """
    Return True if the DNA string only contains characters
    a, c, t, or g (lower or uppercase). False otherwise.
    """
    win = 1
    for i in h:
        if i not in ("actgACTG"):
            win = 0
        if win == 1:
            return True

def base_pair(j):
    """
    Return the corresponding character (lowercase)
    of the base pair. If the base is not recognized,
    return 'unknown'.
    """
    if j not in ("actgACTG"):
        return("unknown")
    else:
        translation = j.maketrans("actgACTG", "tgactgac")
        return j.translate(translation)

def transcribe_dna_to_rna(k):
    k = k.upper()
    k = k.replace("T", "U")
    return k
        
def get_complement(l):
    translation = l.maketrans("actgACTG", "TGACTGAC")
    return l.translate(translation)

def get_reverse_complement(m):
    translation = m.maketrans("actgACTG", "TGACTGAC")
    m = m.translate(translation)
    return m[::-1]

def remove_substring(n, o):
    return o.replace(n, "")

def get_position_indices(p, q):
    indices = []
    q = [q[i:i+3] for i in range(0, len(q), 3)]
    for i in q:
        if i == p:
            indices.append(q.index(i))
    return indices
